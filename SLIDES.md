---
title: Barenaked Ladies
revealOptions:
    defaultTiming: 20
---

> If I had $1,000,000...I'd still eat Kraft Dinner

the barenaked ladies story

---

# 1988

Note: the story starts in 1988

---

![](https://upload.wikimedia.org/wikipedia/commons/2/29/StevenPageMassey.jpg)
![](https://upload.wikimedia.org/wikipedia/commons/7/75/EdRob1.jpg)

steven & ed

Note: Steven Page and Ed Robertson went to school together, but became
friends after meeting after a Peter Gabriel concert. They share guitar and vocal duties. They would later add Jim
& Andy Creegan (bass, keyboards), Tyler Stewart (percussions).

---

![](https://upload.wikimedia.org/wikipedia/en/2/2c/Gordon-album.jpg)

1992

Note: Gordon was their first full albumn, with some their most well-known
songs (Enid, Be My Yoko Ono, Brian Wilson, and If I Had $1,000,000). It went
platinum in Canada.

---

![](https://upload.wikimedia.org/wikipedia/en/a/ad/Barenaked_Ladies_-_Maybe_You_Should_Drive.jpg)

1994

Note: their follow up, Maybe you should drive, was less popular. Andy Creegan
got fed up, left and was replaced on keyboards by Kevin Hearn.

---

![](https://upload.wikimedia.org/wikipedia/en/9/9d/Barenaked_Ladies_-_Born_on_a_Pirate_Ship.jpg)
![](https://upload.wikimedia.org/wikipedia/en/5/51/Barenaked_Ladies_-_Rock_Spectacle.jpg)

1996

Note: Born on Pirate and Rock Spectacle set the stage for them to hit it big
in the US. "Shoe box" off Born on a Pirate ship was on the Friends
soundtrack, and the live version of "Brian Wilson" off Rock Spectacle was a minor US hit.

---

![](https://upload.wikimedia.org/wikipedia/en/4/47/Barenaked_Ladies_-_Stunt.jpg)

1998

Note: their most poular album, Stunt, released in 1998. The single "One week"
spent an appropriate one week at #1 on the Billboard hot 100, and the album
peaked at #3. this would be the peak of their popularity in the united
states.

---

![](https://upload.wikimedia.org/wikipedia/en/4/49/Barenaked_Ladies_-_Maroon.jpg)
![](https://upload.wikimedia.org/wikipedia/en/1/16/BarenakedLadiesDiscOneAllTheirGreatestHits19912001.jpg)

2000-1

Note: In 2000, they were the the worlds bet selling canadian group. the
single "pinch me" of maroon reached #5 on the billboard hot 100, and they
released a greatest hit album the next year.

---

![](https://upload.wikimedia.org/wikipedia/en/7/7b/The_Big_Bang_Theory_%28Official_Title_Card%29.png)

Note: Ed wrote the "the history of everything", theme to the hit TV show Big Bang Theory in 2007, and Steven would leave in 2009.

---

![](https://www.dropbox.com/s/1zpjgv0o183ylom/Screenshot%202020-04-30%2018.04.18.png?dl=1)

Note: while their best days are in the past, they continue to tour. They
opened last year for Hootie and the Blowfish, and had a tour that was
planned for this year with the Gin Blossoms and Toad the Wet Sprocket.

---

![](https://upload.wikimedia.org/wikipedia/en/5/54/Billboard-music-awards-logo.png)
![](https://upload.wikimedia.org/wikipedia/en/e/ea/2018_Juno_Awards_Logo_White_BAckground.jpg)
![](https://upload.wikimedia.org/wikipedia/en/7/77/Grammy_Award_2002.jpg)

Note: they've won 2 billboard music awards, 8 juno awards, and were nominated
twice for a grammy award. in 2018, they were inducted into the canadian music
hall of fame.

---

![](https://www.benandjerrys.ca/files/live/sites/systemsite/files/flavors/products/ca/pints/if-i-had-en-detail.png)

Note: their most important award is an ice cream from Ben & Jerrys: If I had
$1,000,000 flavours, with vanilla and chocolate ice cream, peanut butter
cups, chocolate-coated toffee chunks, white chocolate chunks, and
chocolate-coated almonds. They were the first canadian band to receive that
honor.

---

![](https://assets.kraftfoods.com/ecomm/kraft-canada/product-image/400x400/00068100058611_a1c1.jpg)
